﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ASP_NET_Razor_com_Helpers.Models
{
    public class Pessoa
    {
        [Display(Name="Nome da pessoa")]
        [Required(ErrorMessage="É obrigatório informar o nome")]
        [DataType(DataType.Text)]
        public string Nome { get; set; }

        [Display(Name = "Data de nascimento")]
        [DataType(DataType.Date)]
        public DateTime DataDeNascimento { get; set; }

        [Display(Name = "E-mail")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Display(Name = "Peso")]
        [DataType(DataType.Text)]
        public int Peso { get; set; }

        [Display(Name = "É casada?")]        
        public bool IsCasada { get; set; }
    }
}